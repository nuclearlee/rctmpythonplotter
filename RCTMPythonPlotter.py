# this should work for beginner, intermediate and advanced
# because you specify the repNumber and hold for each rep
class HoldSet:
    def __init__(self, holdName=None, repNumber=0, bodyWeight=0, counterWeight=0, TUT=0):
        self.holdName = holdName
        # I'm labeling the reps 1 and 2 (not 0 and 1), mainly because I'm too lazy to
        self.repNumber = repNumber
        self.counterWeight = counterWeight
        self.TUT = TUT

class WorkOut():
    def __init__(self, workoutNumber=0, bodyWeight=0.0, volume=0.0):
        self.workoutNumber = workoutNumber
        self.bodyWeight = bodyWeight
        self.volume = volume
        self.Sets = []

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties

# todo : read these from the header of the log file
holds = ["LEI", "MR", "SEO", "IMR", "MP", "SL", "SEI"]
print(holds)

f = open('simplelog.txt', 'r')
print(str(f))

labels = f.readline()

workouts = []
maxVolume = 0.0
counter = 0
for line in f:
    # I don't really know either of these with the way I have the file set up right now
    workout = WorkOut(0,0.0,0.0)
    holdSets = []
    counter += 1
    line_list = line.split(' ')
    line_item = 0
    holdsCount = 0
    # DEBUG
    for item in line_list:
        line_item += 1
        if (line_item == 1):
            workoutNumber = line_list[line_item-1]
            workout.workoutNumber = float(workoutNumber)
        elif (line_item == 2):
            bodyWeight = line_list[line_item-1]
            workout.bodyWeight = float(bodyWeight)
        elif (line_item > 2 and line_item <= 16):
            current_hold = HoldSet();
            counterWeight = line_list[line_item-1]
            TUT = line_list[line_item+13]
            current_hold.holdName = holds[holdsCount]
            if (line_item % 2 == 0):
                repNumber = 2
                holdsCount += 1
            else:
                repNumber = 1
            current_hold.repNumber = int(repNumber)
            current_hold.TUT = int(TUT)
            cW = float(counterWeight)
            if (cW != 150.0):
                current_hold.counterWeight = cW
            else:
                current_hold.counterWeight = -workout.bodyWeight
            holdSets.append(current_hold)
            workout.volume += (workout.bodyWeight + current_hold.counterWeight) * current_hold.TUT
        else:
            break

    if (workout.volume > maxVolume):
        maxVolume = workout.volume
    workout.Sets.append(holdSets)
    workouts.append(workout)
    
print("total number of lines"+str(counter))
print("total number of workouts: " + str(len(workouts)))

# now set up the plot for the holds in our file
# I'm guessing I could do this using my list, but I'm going to just
# only plot max weights, so rep2
workoutNumbers = []
lei = []
mr  = []
seo = []
imr = []
mp  = []
sl  = []
sei = []
volumeNorms = []
volumes = []

counter = 0
for workout in workouts:
    workoutNumbers.append(workout.workoutNumber)
    volumeNorms.append(workout.volume/maxVolume)
    volumes.append(workout.volume)
    for set in workout.Sets:
        for hold in set:
            if (hold.holdName == "LEI"):
                if (hold.repNumber == 2):
                    lei.append(workout.bodyWeight + hold.counterWeight)
            elif (hold.holdName == "MR"):
                if (hold.repNumber == 2):
                    mr.append(workout.bodyWeight + hold.counterWeight)
            elif (hold.holdName == "SEO"):
                if (hold.repNumber == 2):
                    seo.append(workout.bodyWeight + hold.counterWeight)
            elif (hold.holdName == "IMR"):
                if (hold.repNumber == 2):
                    imr.append(workout.bodyWeight + hold.counterWeight)
            elif (hold.holdName == "MP"):
                if (hold.repNumber == 2):
                    mp.append(workout.bodyWeight + hold.counterWeight)
                    print("MP cw: "+str(hold.counterWeight))
            elif (hold.holdName == "SL"):
                if (hold.repNumber == 2):
                    sl.append(workout.bodyWeight + hold.counterWeight)
            elif (hold.holdName == "SEI"):
                if (hold.repNumber == 2):
                    sei.append(workout.bodyWeight + hold.counterWeight)

#plt.xkcd()
fig,axarr = plt.subplots(3, sharex=True)

# thanks stack overflow http://stackoverflow.com/questions/8389636/creating-over-20-unique-legend-colors-using-matplotlib
NUM_COLORS = 10
# matplotlib < 1.5
#cm = plt.get_cmap('gist_rainbow')
# deprecated in matplotlib 1.5, gotta figure out how to use set_prop_cycle
#axarr[0].set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

# matplotlib >= 1.5
axarr[0].set_prop_cycle(plt.cycler('color', plt.cm.Accent(np.linspace(0, 1, NUM_COLORS))))


axarr[0].plot(workoutNumbers, lei, marker='x', label="LEI")
axarr[0].plot(workoutNumbers, mr,  marker='.', label="MR")
axarr[0].plot(workoutNumbers, seo, marker='.', label="SEO")
axarr[0].plot(workoutNumbers, imr, marker='.', label="IMR")
axarr[0].plot(workoutNumbers, mp,  marker='.', label="MP")
axarr[0].plot(workoutNumbers, sl,  marker='.', label="SL")
axarr[0].plot(workoutNumbers, sei, marker='.', label="SEI")

# well, I don't know how the eff this works. legend control is stupid
fontP = FontProperties()
# from https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
fontP.set_size('small')
box = axarr[0].get_position()
axarr[0].set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 1.1])
legend = axarr[0].legend(prop=fontP, loc='upper center', bbox_to_anchor=(0.5, 1.25),
          fancybox=True, shadow=True, ncol=7)
#frame = legend.get_frame()
#frame.set_facecolor('0.90')

axarr[1].plot(workoutNumbers, volumes, marker='.')
axarr[2].plot(workoutNumbers, volumeNorms, marker='.')


axarr[0].set_ylabel("Net Weight (lbs)")
axarr[1].set_ylabel("Volume (lb * s)")
axarr[2].set_ylabel("Normalized Volume")


plt.xlabel("Workout number")
plt.show()
